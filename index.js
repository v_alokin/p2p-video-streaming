

var path = require('path')

var app = require('express')();
var server = require('http').createServer(app);
var io = require('socket.io')(server);


// the list of users
users = [];



app.get('/', function(req, res) {
    res.sendFile(path.join(path.dirname(__filename) + '/index.html'))
})



io.on('connection', function(socket){
    console.log('a user connected');


    socket.on('registration', function(user) {

        users.push(user);
        console.log(users);
    });

    socket.on('disconnect', function(){
        console.log('user disconnected');
      });
 });





server.listen(3000);
console.log("Working...")